package gr.apphub.chaniaguide;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;

import java.util.ArrayList;

import gr.apphub.chaniaguide.Adapters.MainListAdapter;
import gr.apphub.chaniaguide.Objects.ListItem;

/**
 * Created by konstantinos on 30/01/2017.
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //init fresco


        //create an arraylist to store the main list items
        final ArrayList<ListItem> listItems = new ArrayList<>();
        listItems.add(new ListItem("Sights", "http://www.apphub.gr/icons/256/Devices/Camera.png", "#d56923"));
        listItems.add(new ListItem("Entertainment", "http://www.apphub.gr/icons/256/Food-Drink/Beer.png", "#e8c019"));
        listItems.add(new ListItem("Events", "http://www.apphub.gr/icons/256/Web/Calendar.png", "#d1372a"));
        listItems.add(new ListItem("News", "http://www.apphub.gr/icons/256/Audio/Sound%20Wave.png", "#283c4b"));
        listItems.add(new ListItem("Information", "http://www.apphub.gr/icons/256/Space/Earth.png", "#2a7eb7"));
        listItems.add(new ListItem("Map", "http://www.apphub.gr/icons/256/Location/Map.png", "#99c37c"));


        ListView listView = (ListView) findViewById(R.id.listView);

        //create the list adapter
        MainListAdapter adapter = new MainListAdapter(this, listItems);
        //set adapter for the listview
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                switch (position) {
                    case 0:
                        Intent sightsIntent = new Intent(MainActivity.this, SightActivity.class);
                        startActivity(sightsIntent);
                        break;

                    default:
                        Toast.makeText(MainActivity.this, "The item " + listItems.get(position).getName() + " is not ready yet!", Toast.LENGTH_SHORT).show();

                        break;
                }
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.map) {
            try {
                Intent myIntent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("geo:35.514129,24.020802?z=14"));
                startActivity(myIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(MainActivity.this, "The map isn't installed on the device!",
                        Toast.LENGTH_SHORT).show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
