package gr.apphub.chaniaguide.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;

import gr.apphub.chaniaguide.Objects.ListItem;
import gr.apphub.chaniaguide.R;

/**
 * Created by Konstantinos on 23/01/17.
 */

public class MainListAdapter extends ArrayAdapter<ListItem> {
    public MainListAdapter(Context context, ArrayList<ListItem> listItems) {
        super(context, 0, listItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ListItem listItem = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.main_list_item, parent, false);
        }
        // Lookup view for data population
        TextView listItem_TextView = (TextView) convertView.findViewById(R.id.listItem_TextView);
        SimpleDraweeView listItem_SimpleDraweeView = (SimpleDraweeView) convertView.findViewById(R.id.listItem_SimpleDraweeView);
        View line = (View) convertView.findViewById(R.id.line);

        // Populate the data into the template view using the data object
        listItem_TextView.setText(listItem.getName());
        listItem_SimpleDraweeView.setImageURI(listItem.getImage());


        line.setBackgroundColor(Color.parseColor(listItem.getLineColor()));

        // Return the completed view to render on screen
        return convertView;
    }
}
