package gr.apphub.chaniaguide.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import gr.apphub.chaniaguide.Objects.Sight;
import gr.apphub.chaniaguide.R;

/**
 * Created by Konstantinos on 23/01/17.
 */

public class SightsAdapter extends ArrayAdapter<Sight> {

    private Context mContext;
    ArrayList<Sight> listItems;

    public SightsAdapter(Context context, ArrayList<Sight> listItems) {
        super(context, 0, listItems);
        this.listItems=listItems;
        this.mContext=context;
    }



    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Sight getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Sight sight = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.sights_list_item, parent, false);
        }
        // Lookup view for data population
        TextView textView = (TextView) convertView.findViewById(R.id.textView);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);


        // Populate the data into the template view using the data object
        textView.setText(sight.getName());

        String sightImage = sight.getImage();
        int imageResource = mContext.getResources().getIdentifier(sightImage, null, mContext.getPackageName());
        Drawable image = mContext.getResources().getDrawable(imageResource);


        imageView.setImageDrawable(image);

        // Return the completed view to render on screen
        return convertView;
    }
}
