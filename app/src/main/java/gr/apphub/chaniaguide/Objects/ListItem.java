package gr.apphub.chaniaguide.Objects;

/**
 * Created by Konstantinos on 23/01/17.
 */

public class ListItem {

    String name;
    String image;
    String lineColor;

    public ListItem() {
    }

    public ListItem(String name, String image, String lineColor) {
        this.name = name;
        this.image = image;
        this.lineColor = lineColor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLineColor() {
        return lineColor;
    }

    public void setLineColor(String lineColor) {
        this.lineColor = lineColor;
    }
}
