package gr.apphub.chaniaguide.Objects;

/**
 * Created by konstantinos on 28/01/2017.
 */

public class Sight {

    public String name;
    public String phone;
    public String description;
    public String locationX;
    public String locationY;
    public String type;
    public String image;


    public Sight(String name, String phone, String description, String locationX, String locationY, String type, String image) {
        this.name = name;
        this.phone = phone;
        this.description = description;
        this.locationX = locationX;
        this.locationY = locationY;
        this.type = type;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocationX() {
        return locationX;
    }

    public void setLocationX(String locationX) {
        this.locationX = locationX;
    }

    public String getLocationY() {
        return locationY;
    }

    public void setLocationY(String locationY) {
        this.locationY = locationY;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
