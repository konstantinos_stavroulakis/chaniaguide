package gr.apphub.chaniaguide;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by konstantinos on 30/01/2017.
 */

public class OpenSight extends AppCompatActivity {

    String name, phone, desc, image, locX, locY, category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_sight);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false); //optional
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        Button mapButton = (Button) findViewById(R.id.mapButton);
        TextView text = (TextView) findViewById(R.id.text);
        TextView cat = (TextView) findViewById(R.id.category);


        Intent i = getIntent();
        name = i.getStringExtra("name");
        phone = i.getStringExtra("phone");
        desc = i.getStringExtra("desc");
        image = i.getStringExtra("image");
        locX = i.getStringExtra("locX");
        locY = i.getStringExtra("locY");
        category = i.getStringExtra("type");


        toolbar.setTitle(name);

        int imageResource = this.getResources().getIdentifier(image, null, getPackageName());
        final Drawable image1 = getResources().getDrawable(imageResource);

        imageView.setImageDrawable(image1);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageDialog(image1);
            }
        });


        text.setText(desc);
        cat.setText(category);

        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //GIA NA ANOIKSEI TO SIMEIO STON XARTI
                    Intent myIntent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("geo:" + locX + "," + locY + "?z=20"));
                    startActivity(myIntent);


                } catch (ActivityNotFoundException e) {
                    Toast.makeText(OpenSight.this, "The map isn't installed on the device!",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });


        Button callButton = (Button) findViewById(R.id.callButton);
        callButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                call();
            }

        });
    }


    public void call() {
        try {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + phone));
            startActivity(callIntent);
        } catch (ActivityNotFoundException e) {
            Log.e("CALL", "Call failed", e);
        }
    }


    public void imageDialog(Drawable image) {

        Dialog dialog = new Dialog(OpenSight.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.imagedialog);

        dialog.setCancelable(true);


        ImageView dialogImageView = (ImageView) dialog.findViewById(R.id.dialogImageView);
        dialogImageView.setImageDrawable(image);

        dialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:

                finish();
                break;

        }


        return super.onOptionsItemSelected(item);
    }


}
