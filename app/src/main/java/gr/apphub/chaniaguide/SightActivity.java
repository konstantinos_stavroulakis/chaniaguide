package gr.apphub.chaniaguide;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import gr.apphub.chaniaguide.Adapters.SightsAdapter;
import gr.apphub.chaniaguide.Objects.Sight;

/**
 * Created by Konstantinos on 23/01/17.
 */
public class SightActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sights_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false); //optional
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle(getString(R.string.sights));

        //create an arraylist to store the main list items
        final ArrayList<Sight> sights = new ArrayList<>();
        sights.add(new Sight("The Egyptian Lighthouse", "2821012345", getText(R.string.faros).toString(), "35.519583", "24.016864", "Ottoman Monuments", "drawable/faros"));
        sights.add(new Sight("Giali Tzamisi", "2821012345", getText(R.string.giali).toString(), "35.5175", "24.017835", "Ottoman Monuments", "drawable/giali"));
        sights.add(new Sight("The Firka Fortress", "2821012345", getText(R.string.firka).toString(), "35.51802", "24.023967", "Venetian Monuments", "drawable/firka1"));
        sights.add(new Sight("The Municipal Market", "2821012345", getText(R.string.agora).toString(), "35.514129", "24.020802", "Modern Monuments", "drawable/agora"));
        sights.add(new Sight("The court-Administration House", "2821012345", getText(R.string.dikastiria).toString(), "35.509798", "24.030688", "Modern Monuments", "drawable/dikastiria"));
        sights.add(new Sight("The center of Meditteranean Architecture(Grand Arsenal)", "2821012345", getText(R.string.arsenali).toString(), "35.518504", "24.019943", "Venetian Monuments", "drawable/arsenali2"));
        sights.add(new Sight("The Venetian Neoria", "2821012345", getText(R.string.neoria).toString(), "35.518225", "24.020872", "Venetian Monuments", "drawable/newria"));
        sights.add(new Sight("Venizelos Tombs", "2821012345", getText(R.string.tombs).toString(), "35.518273", "24.03918", "Modern Monuments", "drawable/tombs"));
        sights.add(new Sight("The Municipal Garden", "2821012345", getText(R.string.kipos).toString(), "35.512391", "24.024047", "Modern Monuments", "drawable/kipos"));
        sights.add(new Sight("The Municipal Clock", "2821012345", getText(R.string.roloi).toString(), "35.512343", "24.02526", "Modern Monuments", "drawable/roloi"));
        sights.add(new Sight("The Venetian Walls", "2821012345", getText(R.string.walls).toString(), "35.518744", "24.014461", "Venetian Monuments", "drawable/walls"));
        sights.add(new Sight("The 1821 Plateau(Splantzia)", "2821012345", getText(R.string.splatzia).toString(), "35.516146", "24.021494", "Ottoman Monuments", "drawable/agnikolas"));


        GridView grid_view = (GridView) findViewById(R.id.grid_view);

        SightsAdapter adapter = new SightsAdapter(this, sights);
        grid_view.setAdapter(adapter);


        grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                Toast.makeText(SightActivity.this, "You clicked at item " + sights.get(position).getName(), Toast.LENGTH_SHORT).show();


                Intent sightsIntent = new Intent(SightActivity.this, OpenSight.class);
                sightsIntent.putExtra("name", sights.get(position).getName());
                sightsIntent.putExtra("phone", sights.get(position).getPhone());
                sightsIntent.putExtra("desc", sights.get(position).getDescription());
                sightsIntent.putExtra("locX", sights.get(position).getLocationX());
                sightsIntent.putExtra("locY", sights.get(position).getLocationY());
                sightsIntent.putExtra("type", sights.get(position).getType());
                sightsIntent.putExtra("image", sights.get(position).getImage());

                startActivity(sightsIntent);
//

            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:

                finish();
                break;

        }


        return super.onOptionsItemSelected(item);
    }
}
